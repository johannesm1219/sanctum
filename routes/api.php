<?php

use App\Http\Controllers\AgentController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\EnduserRequestController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LevelController;
use App\Http\Controllers\OrganizationController;
use App\Http\Controllers\PlasticController;
use App\Http\Controllers\RechargePointController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\RecyclerRequestController;
use App\Http\Controllers\NotificationController;

//protected routes
Route::group(['middleware' => ['auth:sanctum']], function(){
    Route::GET('/agents', [AgentController::class, 'index']); //view all agents
    Route::GET('/organizations', [OrganizationController::class, 'index']); //view all organizations
});

// Authentication
Route::POST('/agents/login', [AgentController::class, 'login']); // Agent login
Route::POST('/organizations/login', [OrganizationController::class, 'login']); // Organization login
Route::POST('/admin/login', [EmployeeController::class, 'login']); // Organization login
Route::GET('/logout', [AuthController::class, 'logout']); //user logout

// Organizations
// Route::GET('/organizations', [OrganizationController::class, 'index']); //view all organizations
Route::GET('/organizations/{organization}', [OrganizationController::class, 'show']); //view specific organization
Route::POST('/organizations', [OrganizationController::class, 'store']); // register an organization

// Agents
// Route::GET('/agents', [AgentController::class, 'index']); //view all agents
Route::GET('/agents/{agent}', [AgentController::class, 'show']); //view specific agent
Route::POST('/agents', [AgentController::class, 'store']); //register an agent
Route::PUT('/agents/{agent}', [AgentController::class, 'update']); //update an agent

// Employee
Route::GET('/employees', [EmployeeController::class, 'index']); //view an Employee
Route::GET('/employees/{employee}', [EmployeeController::class, 'show']); //show an agent
Route::POST('/employees', [EmployeeController::class, 'store']); //register an agent
Route::PATCH('/employees/{employee}', [EmployeeController::class, 'update']); //update an agent

// level routes
Route::resource('levels', LevelController::class);

// Role routes
Route::resource('roles', RoleController::class);

// User routes
Route::resource('users', UserController::class);
Route::GET('/users', [UserController::class, 'index']);
Route::POST('/users', [UserController::class, 'register']);
Route::POST('/users/login', [UserController::class, 'login']);

// EnduserRequest Routes
Route::GET('/enduser-requests', [EnduserRequestController::class, 'viewAllRequests']);
Route::POST('/enduser-requests', [EnduserRequestController::class, 'store']);
Route::PUT('/enduser-requests', [EnduserRequestController::class, 'update']);
Route::GET('/enduser-requests/{enduserRequest}', [EnduserRequestController::class, 'show']);
// Route::PUT('/enduser-requests/close/{enduserRequest}', [EnduserRequestController::class, 'closeRequest']);
Route::PUT('/enduser-requests/cancel', [EnduserRequestController::class, 'cancelRequest']);
Route::PUT('/enduser-requests/accept', [EnduserRequestController::class, 'acceptRequest']);

// plastic routes
Route::resource('plastics', PlasticController::class);

// Recycler Request(Delivery Request) routes
Route::GET('/delivery/pending', [RecyclerRequestController::class, 'pendingRequests'])->name('delivery.pending');
Route::GET('/delivery/accepted', [RecyclerRequestController::class, 'acceptedRequests'])->name('delivery.accepted');
Route::GET('/delivery', [RecyclerRequestController::class, 'index']);
Route::POST('/delivery', [RecyclerRequestController::class, 'store']);
Route::PUT('/delivery', [RecyclerRequestController::class, 'update']);
Route::PUT('/delivery/cancel', [RecyclerRequestController::class, 'cancelDeliveryRequest'])->name('delivery.cancel');
Route::PUT('/delivery/accept', [RecyclerRequestController::class, 'acceptDeliveryRequest']);


// Recharge Point routes
Route::GET('/recharge-point', [RechargePointController::class, 'index']);
Route::POST('/recharge-point', [RechargePointController::class, 'store']);
Route::GET('/recharge-point/{rechargePoint}', [RechargePointController::class, 'show']);
Route::PUT('/recharge-point/{rechargePoint}', [RechargePointController::class, 'update']);
Route::DELETE('/recharge-point/{rechargePoint}', [RechargePointController::class, 'destroy']);


// Notifications
Route::GET('/notifications', [NotificationController::class, 'index']);
Route::GET('/notifications/{notification}', [NotificationController::class, 'show']);
Route::PUT('/notifications/{notification}', [NotificationController::class, 'update']);
Route::PUT('/notifications/delete/{notification}', [NotificationController::class, 'hideNotification']);
Route::POST('/notifications', [NotificationController::class, 'store']);
