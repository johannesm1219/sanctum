<?php

namespace Database\Factories;

use App\Models\Awareness;
use App\Models\Employee;
use Illuminate\Database\Eloquent\Factories\Factory;

class AwarenessFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Awareness::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'employee_id' => Employee::all()->random()->id,
            'subject' => $this->faker->text(50),
            'body' => $this->faker->text(400),
        ];
    }
}
