<?php

namespace Database\Factories;

use App\Models\Agent;
use App\Models\Organization;
use App\Models\Rating;
use Illuminate\Database\Eloquent\Factories\Factory;

class RatingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Rating::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'agent_id' => Agent::all()->random()->id,
            'rated_by' => Organization::all()->random()->id,
            'value' => $this->faker->numberBetween(1.0, 10.0),
            'comment' => $this->faker->text(100),
        ];
    }
}
