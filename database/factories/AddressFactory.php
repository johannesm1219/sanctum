<?php

namespace Database\Factories;

use App\Models\Address;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'city' => $this->faker->city(),
            'subcity' => $this->faker->city(),
            'woreda' => $this->faker->city(),
            'longitude' => $this->faker->unique()->longitude(),
            'latitude' => $this->faker->unique()->latitude(),
            'specific_location' => $this->faker->city(),
        ];
    }
}
