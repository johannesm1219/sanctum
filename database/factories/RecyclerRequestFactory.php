<?php

namespace Database\Factories;

use App\Models\Agent;
use App\Models\Organization;
use App\Models\Plastic;
use App\Models\RecyclerRequest;
use Illuminate\Database\Eloquent\Factories\Factory;

class RecyclerRequestFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RecyclerRequest::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'recycler_id' => Organization::all()->random()->id,
            'agent_id' => Agent::all()->random()->id,
            'plastic_id' => Plastic::all()->random()->id,
            'group_id' => $this->faker->uuid(),
            'quantity' => $this->faker->numberBetween(10, 100000),
            'price' => $this->faker->numberBetween(10, 100000),
            'unit' => $this->faker->text(5),
            'status' => $this->faker->randomElement(['Pending', 'Accepted', 'Canceled', 'Delivered']),
        ];
    }
}
