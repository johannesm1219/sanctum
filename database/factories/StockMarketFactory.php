<?php

namespace Database\Factories;

use App\Models\Employee;
use App\Models\Plastic;
use App\Models\StockMarket;
use Illuminate\Database\Eloquent\Factories\Factory;

class StockMarketFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StockMarket::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'plastic_id' => Plastic::all()->random()->id,
            'employee_id' => Employee::all()->random()->id,
            'quantity' => $this->faker->numberBetween(10, 100000),
            'price' => $this->faker->numberBetween(10, 100000),
            'description' => $this->faker->text(100),
        ];
    }
}
