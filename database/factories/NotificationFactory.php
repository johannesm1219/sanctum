<?php

namespace Database\Factories;

use App\Models\Agent;
use App\Models\Employee;
use App\Models\Notification;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class NotificationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Notification::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'subject' => $this->faker->text(30),
            'body' => $this->faker->text(10),
            'role_id' => Role::all()->random()->id,
            'start_date' => Carbon::now(),
            'end_date' => Carbon::tomorrow(),
            'status' => $this->faker->numberBetween(0, 1),
        ];
    }
}
