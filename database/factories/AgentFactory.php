<?php

namespace Database\Factories;

use App\Models\Address;
use App\Models\Agent;
use App\Models\Level;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class AgentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Agent::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'level_id' => Level::all()->random()->id,
            'address_id' => Address::all()->random()->id,
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => Hash::make('password'), //password
            'phone' => $this->faker->phoneNumber(),
            'vehicle_description' => $this->faker->text(100),
            'kebele_id' => $this->faker->text(100),
            'libre' => $this->faker->text(100),
            'agent_type' => $this->faker->text(100),
        ];
    }
}
