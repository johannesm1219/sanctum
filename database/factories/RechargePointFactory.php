<?php

namespace Database\Factories;

use App\Models\Agent;
use App\Models\Employee;
use App\Models\RechargePoint;
use Illuminate\Database\Eloquent\Factories\Factory;

class RechargePointFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RechargePoint::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'agent_id' => Agent::all()->random()->id,
            'recharged_by' => Employee::all()->random()->id,
            'point_recharged' => $this->faker->numberBetween(10, 100000),
            'current_points' => $this->faker->numberBetween(10, 100000),
        ];
    }
}
