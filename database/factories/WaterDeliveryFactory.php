<?php

namespace Database\Factories;

use App\Models\WaterDelivery;
use Illuminate\Database\Eloquent\Factories\Factory;

class WaterDeliveryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = WaterDelivery::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
