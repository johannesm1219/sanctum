<?php

namespace Database\Factories;

use App\Models\Agent;
use App\Models\EnduserRequest;
use App\Models\Organization;
use App\Models\Plastic;
use Illuminate\Database\Eloquent\Factories\Factory;

class EnduserRequestFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EnduserRequest::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'enduser_id' => Organization::all()->random()->id,
            'agent_id' => Agent::all()->random()->id,
            'plastic_id' => Plastic::all()->random()->id,
            'group_id' => $this->faker->uuid(),
            'quantity' => $this->faker->numberBetween(100, 10000),
            'unit' => $this->faker->text(5),
            'status' => $this->faker->numberBetween(0, 2),
        ];
    }
}
