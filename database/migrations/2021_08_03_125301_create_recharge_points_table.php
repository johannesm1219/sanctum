<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRechargePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recharge_points', function (Blueprint $table) {
            $table->uuid('id', 255)->primary();
            $table->foreignUuid('agent_id', 255)->constrained('agents'); // agent id from user/organization table
            $table->foreignUuid('recharged_by', 255)->constrained('employees'); // admin id from Employee table 
            $table->decimal('point_recharged');
            $table->decimal('current_points');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recharge_points');
    }
}
