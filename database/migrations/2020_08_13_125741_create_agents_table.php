<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agents', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->foreignUuid('role_id')->default('22376529-e990-4e75-a259-c5c565b7f51e')->constrained('roles');
            $table->foreignUuid('level_id')->constrained('levels');
            $table->foreignUuid('address_id')->constrained('addresses');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('gender')->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('password');
            $table->text('vehicle_description')->nullable();
            $table->text('plate_number')->nullable();
            $table->string('phone', 15)->unique();
            $table->string('kebele_id')->nullable();
            $table->string('libre')->nullable();
            $table->string('agent_type')->default('personal');
            $table->integer('status')->default(0); //0: registration pending, 1: active, 2: account deactivated
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agents');
    }
}
