<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->uuid('id', 255);
            $table->string('subject');
            $table->text('body');
            $table->foreignUuid('role_id', 255)->constrained('roles');
            $table->date('start_date')->default(Carbon::now());
            $table->date('end_date')->nullable();
            $table->integer('status')->default(1); //1: active, 0:inactive
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
