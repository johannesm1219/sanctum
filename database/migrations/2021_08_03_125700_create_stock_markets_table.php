<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStockMarketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_markets', function (Blueprint $table) {
            $table->uuid('id', 255)->primary();
            $table->foreignUuid('plastic_id', 255)->constrained('plastics');
            $table->foreignUuid('employee_id', 255)->constrained('employees');
            $table->decimal('quantity');
            $table->decimal('price');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_markets');
    }
}
