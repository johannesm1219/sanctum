<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnduserRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enduser_requests', function (Blueprint $table) {
            $table->uuid('id', 255)->primary();
            $table->foreignUuid('enduser_id', 255)->constrained('organizations');
            $table->foreignUuid('agent_id', 255)->nullable()->constrained('agents');
            $table->foreignUuid('plastic_id', 255)->constrained('plastics');
            $table->uuid('group_id', 255);
            $table->decimal('quantity')->nullable();
            $table->string('unit')->nullable();
            $table->integer('status')->default(0); // 0:open, 1:closed, 2:canceled
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enduser_requests');
    }
}
