<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecyclerRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recycler_requests', function (Blueprint $table) {
            $table->uuid('id', 255)->primary();
            $table->foreignUuid('recycler_id', 255)->constrained('organizations');
            $table->foreignUuid('agent_id', 255)->nullable()->constrained('agents');
            $table->foreignUuid('plastic_id', 255)->constrained('plastics');
            $table->uuid('group_id', 255);
            $table->decimal('quantity');
            $table->decimal('price');
            $table->string('unit');
            $table->enum('status', ['Pending', 'Accepted', 'Canceled', 'Delivered'])->default('Pending'); // 0:open, 1:closed, 2:canceled
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recycler_requests');
    }
}
