<?php

namespace Database\Seeders;

use App\Models\Awareness;
use Illuminate\Database\Seeder;

class AwarenessSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Awareness::factory()->times(10)->create();
    }
}
