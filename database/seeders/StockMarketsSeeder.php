<?php

namespace Database\Seeders;

use App\Models\StockMarket;
use Illuminate\Database\Seeder;

class StockMarketsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StockMarket::factory()->times(20)->create();
    }
}
