<?php

namespace Database\Seeders;

use App\Models\Level;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LevelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('levels')->insert(array(
            0 => 
            array(
                'id' => '08396b6c-f60d-47e7-ba55-bd7b0e91bd4b',
                'name' => 'Level 1',
                'description' => 'this is level one',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
        ));
        DB::table('levels')->insert(array(
            0 => 
            array(
                'id' => '0ee465da-5d5a-4f5a-9d7d-9b510ef0c313',
                'name' => 'Level 2',
                'description' => 'this is level two',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
        ));
        DB::table('levels')->insert(array(
            0 => 
            array(
                'id' => '49a6f49d-6efc-4fcd-a44a-35b49c2e015c',
                'name' => 'Level 3',
                'description' => 'this is level three',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
        ));
        DB::table('levels')->insert(array(
            0 => 
            array(
                'id' => '4332eca8-ff6a-4e0c-b593-dbc7ec52197e',
                'name' => 'Level 4',
                'description' => 'this is level four',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
        ));
        DB::table('levels')->insert(array(
            0 => 
            array(
                'id' => '2cabd516-7a58-4b54-8453-8d7b4ff3f84f',
                'name' => 'Level 5',
                'description' => 'this is level five',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
        ));
        DB::table('levels')->insert(array(
            0 => 
            array(
                'id' => '163ee230-6f18-4da7-86cc-b039c8f5a400',
                'name' => 'Level 6',
                'description' => 'this is level six',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
        ));
    }
}
