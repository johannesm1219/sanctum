<?php

namespace Database\Seeders;

use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(array(
            0 => 
            array(
                'id' => '0d1965f5-761d-49de-bb75-750072f4da5b',
                'name' => 'Admin',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
        ));
        DB::table('roles')->insert(array(
            0 => 
            array(
                'id' => '22376529-e990-4e75-a259-c5c565b7f51e',
                'name' => 'Agent',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
        ));
        DB::table('roles')->insert(array(
            0 => 
            array(
                'id' => '561eca15-80ae-4214-b5e6-005f20f48586',
                'name' => 'Recycler',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
        ));
        DB::table('roles')->insert(array(
            0 => 
            array(
                'id' => 'f4e57a5f-8645-41ad-b6c3-10411cee9d4b',
                'name' => 'EndUser',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
        ));

        DB::table('roles')->insert(array(
            0 => 
            array(
                'id' => '1a7e7c4e-e4d9-45e1-8aa9-6d050c3911fe',
                'name' => 'Factory',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
        ));
    }
}
