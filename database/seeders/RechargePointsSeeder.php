<?php

namespace Database\Seeders;

use App\Models\RechargePoint;
use Illuminate\Database\Seeder;

class RechargePointsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RechargePoint::factory()->times(20)->create();
    }
}
