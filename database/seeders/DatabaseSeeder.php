<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesSeeder::class);
        $this->call(LevelsSeeder::class);
        $this->call(PlasticsSeeder::class);
        $this->call(AddressesSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(EmployeeSeeder::class);
        $this->call(AgentSeeder::class);
        $this->call(OrganizationsSeeder::class);
        $this->call(EnduserRequestSeeder::class);
        $this->call(RecyclersRequestSeeder::class);
        $this->call(AwarenessSeeder::class);
        $this->call(RatingsSeeder::class);
        $this->call(NotificationsSeeder::class);
        $this->call(RechargePointsSeeder::class);
        $this->call(StockMarketsSeeder::class);
    }
}
