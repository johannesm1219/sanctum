<?php

namespace Database\Seeders;

use App\Models\Plastic;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlasticsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plastics')->insert(array(
            0 => 
            array(
                'id' => '1309acbb-7736-4da3-a76b-3af828daa95f',
                'name' => 'PET/PETE',
                'type' => '1',
                'alternative_name' => 'Polyethylene Terephthalate',
                'image' => 'plastics/PET.png',
                'description' => 'water bottles , Beverage bottles , Jelly jars ...',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
            1 => 
            array(
                'id' => '3caf7885-a9ce-4d6e-8ce4-19c0e2e53a90',
                'name' => 'HDPE',
                'type' => '2',
                'alternative_name' => 'High-Density Polyethylene',
                'image' => 'plastics/HDPE.png',
                'description' => 'Containers, Shampoo and conditioner bottles...',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
            2 => 
            array(
                'id' => '4757fdf6-f4d6-4ef9-84e8-454cc56e2725',
                'name' => 'PVC/Vinyl',
                'type' => '3',
                'alternative_name' => 'Polyvinyl Chloride',
                'image' => 'plastics/PVC.png',
                'description' => 'Plumbing pipes, Tile, Sewage pipes, Gutters...',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
            3 => 
            array(
                'id' => '52b0f4c4-5e26-47a6-84fc-f65443fb590d',
                'name' => 'LDPE',
                'type' => '4',
                'alternative_name' => 'Low-Density Polyethylene',
                'image' => 'plastics/LDPE.png',
                'description' => 'Grocery bags, Squeezable bottles, Cling wrap...',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
            // 4 => 
            // array(
            //     'id' => '55771b6b-f685-47dc-ae4f-d0da3cca35e5',
            //     'name' => 'PP',
            //     'type' => '5',
            //     'alternative_name' => 'Polypropylene',
            //     'description' => 'This is one of the most durable types of plastic. It is more heat resistant than some others, which makes it ideal for such things as food packaging and food storage that’s made to hold hot items or be heated itself. It’s flexible enough to allow for mild bending, but it retains its shape and strength for a long time. Examples: Straws, bottle caps, prescription bottles, hot food containers, packaging tape, disposable diapers and DVD/CD boxes (remember those!).',
            //     'created_at' => Carbon::now(),
            //     'updated_at' => Carbon::now(),
            // ),
            // 5 => 
            // array(
            //     'id' => '6202699c-8006-4ba0-9bb4-1363364c0fd8',
            //     'name' => 'PS',
            //     'type' => '6',
            //     'alternative_name' => 'Polystyrene',
            //     'description' => 'Egg cartons, cutlery and building insulation.',
            //     'created_at' => Carbon::now(),
            //     'updated_at' => Carbon::now(),
            // ),
            4 => 
            array(
                'id' => '79ea3722-ee59-4079-8673-8e3a882e0d17',
                'name' => 'Mixed',
                'type' => '7',
                'alternative_name' => 'Other',
                'image' => 'plastics/Unsorted.png',
                'description' => 'Unsorted or the mix of the above mentioned...',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
        ));
    }
}
