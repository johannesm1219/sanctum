<?php

namespace Database\Seeders;

use App\Models\RecyclerRequest;
use Illuminate\Database\Seeder;

class RecyclersRequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RecyclerRequest::factory()->times(20)->create();
    }
}
