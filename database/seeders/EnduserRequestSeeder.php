<?php

namespace Database\Seeders;

use App\Models\EnduserRequest;
use Illuminate\Database\Seeder;

class EnduserRequestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EnduserRequest::factory()->times(15)->create();
    }
}
