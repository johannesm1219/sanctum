<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        Route::middleware('auth.admin')->group(function(){
            Route::middleware('auth: sanctum')->group(function(){
                Route::get('/home', [AdminController::class, 'index']);
            });
        });
        return $next($request);
    }
}
