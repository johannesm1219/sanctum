<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\OrganizationLoginRequest;
use App\Http\Requests\Organization\CreateOrganizationRequest;
use App\Models\Address;
use App\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use PhpParser\Node\Stmt\TryCatch;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Organization::all();
    }

    public function login(OrganizationLoginRequest $request)
    {
        $user = (new AuthController)->organizationLogin($request);

        if($user != Null){
            // $token = $user->createToken($user->id)->plainTextToken;
            return response([
                'organization'=>$user,
                // 'token'=>$token,
                'message' => 'Signed successfully.'
            ], 201);
        }else{
            return response([
                'message' => "Credentials are Incorrect"
            ], 401);
        }
    }

    /**
     * Logout from the system using the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function logout(Request $request)
    {
        // return true;
        // auth()->user()->tokens()->delete();

        return response(['message' => "Logged Out"], 201);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateOrganizationRequest $request)
    {
        try {
            $address = (new AddressController)->store($request);
            // $this->storeAvatar($request, $address);
            if($address !== Null){
                // $password = Str::random(12);;
                $password = 'password';
                $request['password'] = $password;
                $organization = $this->storeOrganization($request, $address->id);
            }
            // $token = $organization->createToken($organization->id)->plainTextToken;
            return response([
                'organization' => $organization,
                'address' => $address,
                'password' => $password,
                // 'token' => $token,
                'message' => 'Organization Registered successfully'
            ], 201);
        } catch (\Exception $e) {
            $organization->delete();
            $address->delete();
            return "Sorry, Unable to register your information";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function show(Organization $organization)
    {
        try {
            $address = Organization::find($organization->id)->address;
            return response([
                'organization' => $organization,
                'address' => $address
            ], 201);
        } catch (\Exception $e) {
            // return $e;
            return response([
                "message" => "Unable to find the organization"
            ], 401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Organization $organization)
    {
        $values = $this->validateUpdateOrganizationRequest();
        $organization = Organization::where('id', $organization->id)->update([
            'phone' => $values['phone'],
            'city' => $values['city'],
            'subcity' => $values['subcity'],
            'woreda' => $values['woreda'],
            'longitude' => $values['longitude'],
            'latitude' => $values['latitude'],
            'specific_location' => $values['specific_location'],
            'avatar' => $values['avatar']
        ]);

        $address = Address::where('id', $organization->address_id)->update([
            'phone' => $values['phone'],
            'city' => $values['city'],
            'subcity' => $values['subcity'],
            'woreda' => $values['woreda'],
            'longitude' => $values['longitude'],
            'latitude' => $values['latitude'],
            'specific_location' => $values['specific_location'],
            'avatar' => $values['avatar']
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy(Organization $organization)
    {
        //
    }

    public function validateLoginRequest()
    {
        return request()->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);
    }

    // validate organization registration request
    private function validateUpdateOrganizationRequest()
    {
        return request() -> validate([
            'role_id' => 'required|string',
            'name' => 'required|string',
            'email' => 'email|required|string|unique:organizations,email|unique:agents,email',
            'password' => 'required|string',
            'phone' => 'required|string',
            'city' => 'required|string',
            'subcity' => 'required|string',
            'woreda' => 'required|string',
            'longitude' => 'required|string',
            'latitude' => 'required|string',
            'specific_location' => 'string'
        ]);
    }

    private function storeAddress($values)
    {
        return Address::create([
            'city' => $values['city'],
            'subcity' => $values['subcity'],
            'woreda' => $values['woreda'],
            'longitude' => $values['longitude'],
            'latitude' => $values['latitude'],
            'specific_location' => $values['specific_location'],
        ]); 
    }

    private function storeOrganization($values, $address_id)
    {
        return Organization::create([
            'role_id' => $values['role_id'],
            'address_id' => $address_id,
            'name' => $values['name'],
            'email' => $values['email'],
            'password' => Hash::make($values['password']),
            'phone' => $values['phone'],
        ]);
    }

    private function storeAvatar($address)
    {
        if(request()->has('image')){
            return $address->update([
                    'avatar' => request()->image->store('uploads/organizations', 'public'),
                ]);
        }
    }
}
