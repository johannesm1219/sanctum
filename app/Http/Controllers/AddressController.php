<?php

namespace App\Http\Controllers;

use App\Models\Address;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($request)
    {
        if($request['longitude'] == ''){
            $request['longitude'] = Null;
        }
        if($request['latitude'] == ''){
            $request['latitude'] = Null;
        }

        try {
            return  Address::create([
                'city' => $request['city'],
                'subcity' => $request['subcity'],
                'woreda' => $request['woreda'],
                'longitude' => $request['longitude'],
                'latitude' => $request['latitude'],
                'specific_location' => $request['specific_location']
            ]);
        } catch (\Exception $e) {
            return Null;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function show(Address $address)
    {
        return $address;
    }

    public function update(Request $request, $address)
    {
        if($request['longitude'] == ''){
            $request['longitude'] = Null;
        }
        if($request['latitude'] == ''){
            $request['latitude'] = Null;
        }

        return $address->update($request->all(), $address);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Address  $address
     * @return \Illuminate\Http\Response
     */
    public function destroy(Address $address)
    {
        $address->delete();
    }
}
