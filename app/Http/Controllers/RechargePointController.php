<?php

namespace App\Http\Controllers;

use App\Http\Requests\RechargePoint\CreateRechargePointRequest;
use App\Http\Requests\RechargePoint\UpdateRechargePointRequest;
use App\Models\RechargePoint;
use Illuminate\Http\Request;

class RechargePointController extends Controller
{
    public function index()
    {
        return RechargePoint::all();
    }

    public function store(CreateRechargePointRequest $request)
    {
        try {
            $recharge_point = RechargePoint::create([
                'agent_id' => $request['agent_id'],
                'recharged_by' => $request['recharged_by'],
                'current_points' => '2000',
                'point_recharged' => $request['point_recharged']
            ]);

            return [
                'data' => $recharge_point,
                'message' => 'Point added to your account.'
            ];
        } catch (\Exception $e) {
            return "Unable to recharge your account.";
        }
    }

    public function show(RechargePoint $rechargePoint)
    {
        try {
            return ['data' => $rechargePoint];
        } catch (\Exception $e) {
            return "record doesn't exist.";
        }
    }

    public function update(UpdateRechargePointRequest $request, RechargePoint $rechargePoint)
    {
        try {
            $rechargePoint->update([
                'agent_id' => $request['agent_id'],
                'current_points' => '2000',
                'point_recharged' => $request['point_recharged']
            ]);

            return response([
                'data' => RechargePoint::find($rechargePoint->id),
                'message' => 'Point updated Successfully'
            ], 201);
        } catch (\Exception $e) {
            return "Unable to Update your recharged point.";
        }
    }

    public function destroy(RechargePoint $rechargePoint)
    {
        $rechargePoint->delete();
        return ['message' => 'Point deleted'];
    }
}
