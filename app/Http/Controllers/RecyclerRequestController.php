<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeliveryRequest\CreateDeliveryRequest;
use App\Http\Requests\DeliveryRequest\UpdateDeliveryRequest;
use App\Models\Address;
use App\Models\RecyclerRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RecyclerRequestController extends Controller
{
    public function index()
    {
        try {
            return RecyclerRequest::
                // all('group_id', 'quantity', 'price');
                with([
                    'Organization:id,name,email',
                    'agent:id,first_name,last_name,phone,email', 
                    'plastic:id,name'
                ])
                // ->pluck('group_id', 'quantity', 'price')
                // ->groupBy('group_id')
                // ->join('addresses', 'organizations.address_id', 'addresses.id')
                ->get();
        } catch (\Exception $e) {
            return $e; 
        }
        
    }

    public function pendingRequests()
    {
        return RecyclerRequest::where('status', 0)->get();
    }
// 0911457917 Girum
    public function acceptedRequests()
    {
        return RecyclerRequest::where('status', 1)->get();
    }

    public function store(CreateDeliveryRequest $request)
    {
        try {
            $group_id = Str::uuid()->toString(); 
            // check if group id is already taken or not
            while(RecyclerRequest::where('group_id', $group_id)->first()){
                $group_id = Str::uuid()->toString();
            }
            $requests = $request->toArray();

            // store each request from the array
            foreach($requests as $request){
                RecyclerRequest::create([
                    'recycler_id' => $request['recycler_id'],
                    'plastic_id' => $request['plastic_id'],
                    'quantity' => $request['quantity'],
                    'unit' => $request['unit'],
                    'price' => $request['price'],
                    'group_id' => $group_id,
                ]);
            }

            return response([
                'data' => RecyclerRequest::where('group_id', $group_id)->get(),
                'message' => 'Delivery Request sent Successfully'
            ], 201);
        } catch (\Exception $e) {
            return $e;
            return "Sorry, unable to store your request";
        } 
    }

    public function show(RecyclerRequest $recyclerRequest)
    {
        return ['data' => $recyclerRequest];
    }

    public function update(UpdateDeliveryRequest $request)
    {
        try {
            $requests = $request->toArray();

            // store each request from the array
            foreach($requests as $request){
                $delivery = RecyclerRequest::where('id', $request['request_id'])->first();
                $delivery->update([
                    'plastic_id' => $request['plastic_id'],
                    'quantity' => $request['quantity'],
                    'price' => $request['price'],
                    'unit' => $request['unit'],
                ]);
            }

            return response([
                'message' => 'Delivery Request Updated.'
            ], 201);
        } catch (\Exception $e) {
            return $e;
            return "Sorry, unable to Update your request";
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RecyclerRequest  $recyclerRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(RecyclerRequest $recyclerRequest)
    {
        //
    }

    // cancel a specific request
    public function cancelDeliveryRequest(Request $request)
    {
        try {
            RecyclerRequest::where('group_id', $request->group_id)->update([
                    'status' => 2
                ]);
            return [
                'message' => 'Delivery Request Canceled'
            ];    
        } catch (\Exception $e) {
            return "Unable to cancel the request";
        }        
    }

    public function acceptDeliveryRequest(Request $request)
    {
        try {
            RecyclerRequest::where('group_id', $request->group_id)->update([
                    'status' => 1,
                    'agent_id' => $request->agent_id
                ]);
            return [
                'message' => 'Request accepted'
            ];
        } catch (\Exception $e) {
            return "Unable to accept the request";
        }
    }
}
