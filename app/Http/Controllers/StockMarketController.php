<?php

namespace App\Http\Controllers;

use App\Models\StockMarket;
use Illuminate\Http\Request;

class StockMarketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StockMarket  $stockMarket
     * @return \Illuminate\Http\Response
     */
    public function show(StockMarket $stockMarket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\StockMarket  $stockMarket
     * @return \Illuminate\Http\Response
     */
    public function edit(StockMarket $stockMarket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\StockMarket  $stockMarket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StockMarket $stockMarket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\StockMarket  $stockMarket
     * @return \Illuminate\Http\Response
     */
    public function destroy(StockMarket $stockMarket)
    {
        //
    }
}
