<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::all();
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return "Create User Form";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        try {
            $values = $this->validateRequestRegister();
            $user = User::create([
                'level_id' => $values['level_id'],
                'first_name' => $values['first_name'],
                'last_name' => $values['last_name'],
                'email' => $values['email'],
                'password' => bcrypt($values['password'])
            ]);
            $token = $user->createToken('digital-kuralew'.$user->id)->plainTextToken;

            // return 'Working';
            return [$user, $token];    
        } catch (\Exception $e) {
            return $e;
        }
        
    }

    /**
     * Login to the system using the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function login(Request $request)
    {
        try {
            // return 'Working';
            // check if user credentials are valid 
            $input = $this->validateLoginRequest();
            $user = User::where('email', $input['email'])->first();
            
            if(!$user || !Hash::check($input['password'], $user->password)){
                return response([
                    'message' => "Credentials are Incorrect"
                ], 401);
            }
            // return 'Walla';
            $token = $user->createToken(Str::random(10));

            $response = [
                'user'=>$user,
                'token'=>$token->plainTextToken,
                'message' => 'Signed successfully.'
            ];

            return response($response, 201);    
        } catch (\Exception $e) {
            return $e;
        }
        

    }

    /**
     * Logout from the system using the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function logout(Request $request)
    {
        // auth()->user()->tokens()->delete();

        return response(['message' => "Logged Out"], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->update($this->validateUpdateRequest());
        $message = "Level updated Successfully";

        return [$message, $user];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        $message = "User deleted successfully";
        return $message;
    }

    private function validateRequestRegister()
    {
        return request() -> validate([
            'level_id' => 'required',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|string|unique:users,email|email',
            'password' => 'required|string'
        ]);
    }

    private function validateUpdateRequest()
    {
        return request() -> validate([
            'level_id' => 'required',
            'first_name' => 'required|string',
            'last_name' => 'required|string',
        ]);
    }

    public function validateLoginRequest()
    {
        return request()->validate([
            'email' => 'required|string',
            'password' => 'required|string'
        ]);
    }

    private function checkPassword($user, $password)
    {
        if(Hash::check(bcrypt($password), $user->password)){
            return response([
                'message' => "Credentials are Incorrect"
            ], 401);
        }
    }
}
