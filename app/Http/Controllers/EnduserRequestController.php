<?php

namespace App\Http\Controllers;

use App\Http\Requests\Enduser\storeEnduserRequest;
use App\Http\Requests\PickupRequest\CreatePickupRequest;
use App\Http\Requests\PickupRequest\UpdatePickupRequest;
use App\Models\EnduserRequest;
use CreateEnduserRequestsTable;
use Illuminate\Support\Str;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EnduserRequestController extends Controller
{
    public function viewAllRequests()
    {
        return EnduserRequest::all();
    }

    public function pendingRequests()
    {
        return EnduserRequest::where('status', 0)->get();
    }

    public function acceptedRequests()
    {
        return EnduserRequest::where('status', 1)->get();
    }

    public function canceledRequests()
    {
        return EnduserRequest::where('status', 2)->get();
    }

    // create a new enduser request
    public function store(CreatePickupRequest $request)
    {
        try {
            $requests = $request->toArray();
            $group_id = Str::uuid()->toString(); 
            // check if group id is already taken
            while(EnduserRequest::where('group_id', $group_id)->first()){
                $group_id = Str::uuid()->toString();
            }

            // store each request from the array
            foreach($requests as $request){
                EnduserRequest::create([
                    'enduser_id' => $request['enduser_id'],
                    'plastic_id' => $request['plastic_id'],
                    'quantity' => $request['quantity'],
                    'unit' => $request['unit'],
                    'group_id' => $group_id,
                ]);
            }

            return response([
                'data' => EnduserRequest::where('group_id', $group_id)->get(),
                'message' => 'Pickup Request sent Successfully'
            ], 201);
        } catch (\Exception $e) {
            return "Sorry, unable to store your request";
        }   
    }

    public function show(EnduserRequest $enduserRequest)
    {
        try {
            return EnduserRequest::where('group_id', $enduserRequest->group_id)->get();
        } catch (\Exception $e) {
            // return $e;
            return ['message' => 'Request Doesnt exist.'];
        }
    }

    public function update(UpdatePickupRequest $request)
    {
        try {
            $requests = $request->toArray();

            // store each request from the array
            foreach($requests as $request){
                $pickup = EnduserRequest::where('id', $request['request_id'])->first();
                $pickup->update([
                    'plastic_id' => $request['plastic_id'],
                    'quantity' => $request['quantity'],
                    'unit' => $request['unit'],
                ]);
            }

            return response([
                'message' => 'Pickup Request Updated.'
            ], 201);
        } catch (\Exception $e) {
            // return $e;
            return "Sorry, unable to Update your request";
        }
    }

    public function destroy(EnduserRequest $enduserRequest)
    {
        $enduserRequest->delete();
        return "Deleted Successfully";
    }

    // cancel a specific request
    public function cancelRequest(Request $request)
    {
        try {
            EnduserRequest::where('group_id', $request->group_id)->update([
                    'status' => 2
                ]);
            return [
                'message' => 'Request Canceled'
            ];    
        } catch (\Exception $e) {
            return "Unable to cancel the request";
        }
    }

    // close a specific request
    public function acceptRequest(Request $request)
    {
        try {
            EnduserRequest::where('group_id', $request->group_id)->update([
                    'status' => 1, // return the status
                    'agent_id' => $request->agent_id
                ]);
            return [
                'message' => 'Request accepted'
            ];
        } catch (\Exception $e) {
            return "Unable to accept the request";
        }
    }
}
