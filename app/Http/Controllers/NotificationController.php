<?php

namespace App\Http\Controllers;

use App\Http\Requests\Notification\CreateNotificationRequest;
use App\Http\Requests\Notification\UpdateNotificationRequest;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index()
    {
        return Notification::all();
    }

    public function store(CreateNotificationRequest $request)
    {
        try {
            $notification = Notification::create(request()->all()); 
            return response([
                'data' => $notification,
                'message' => 'Notification sent Successfully'
            ], 201);
        } catch (\Exception $e) {
            // return $e;
            return "Sorry, unable to send your notification";
        }
    }

    public function show(Notification $notification)
    {
        return $notification;
    }


    public function update(UpdateNotificationRequest $request, Notification $notification)
    {
        try {
            $notification->update($request->all());
            return response([
                'data' => Notification::find($notification->id),
                'message' => 'Updated Successfully'
            ], 201);
        } catch (\Exception $e) {
            return $e;
            return ['message' => 'Notification cant be updated successfully'];
        }
    }

    public function deactivateNotification(Notification $notification)
    {
        //
    }

    public function hideNotification(Notification $notification)
    {
        try {
            $notification->update([
                'status' => 2,
            ]);
            return response([
                "data" => $notification,
                "message" => "Notification deleted."
            ], 200);
        } catch (\Exception $e) {
            return 'Sorry, unable to cancel the request';
        }
    }
}
