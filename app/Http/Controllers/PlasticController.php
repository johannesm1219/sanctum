<?php

namespace App\Http\Controllers;

use App\Models\Plastic;
use Illuminate\Http\Request;

class PlasticController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Plastic::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Plastic::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Plastic  $plastic
     * @return \Illuminate\Http\Response
     */
    public function show(Plastic $plastic)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Plastic  $plastic
     * @return \Illuminate\Http\Response
     */
    public function edit(Plastic $plastic)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Plastic  $plastic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Plastic $plastic)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Plastic  $plastic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Plastic $plastic)
    {
        //
    }
}
