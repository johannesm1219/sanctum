<?php

namespace App\Http\Controllers;

use App\Models\Agent;
use App\Models\Employee;
use App\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function adminLogin(Request $request)
    {
        try {
            $admin = Employee::where('email', $request['email'])->first();
            
            if(!$admin || !Hash::check($request['password'], $admin->password)){
                return Null;
            }
            return $admin;
        } catch (\Exception $e) {
            return Null;
        }
    }
    
    public function agentLogin(Request $request)
    {
        try {
            $agent = Agent::where('phone', $request['phone'])->first();
            
            if(!$agent || !Hash::check($request['password'], $agent->password)){
                return Null;
            }
            return $agent;
        } catch (\Exception $e) {
            return Null;
        }
    }

    public function organizationLogin(Request $request)
    {
        try {
            $organization = Organization::where('email', $request['email'])->first();
            
            if(!$organization || !Hash::check($request['password'], $organization->password)){
                return Null;
            }    
            return $organization;
        } catch (\Exception $e) {
            return Null;
        }
    }

    // destroy agent session and logout
    public function logout(Request $request)
    {
        // auth()->user()->tokens()->delete();

        return response(['message' => "Logged Out"], 201);
    }

    // validate organization's login request
    public function validateOrganizationLoginRequest()
    {
        return request()->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
    }

    // validate agent's login request
    public function validateAgentLoginRequest()
    {
        return request()->validate([
            'phone' => 'required|string',
            'password' => 'required|string'
        ]);
    }

    // validate agent's login request
    public function validateAdminLoginRequest()
    {
        return request()->validate([
            'email' => 'required|string|email',
            'password' => 'required|string'
        ]);
    }
}
