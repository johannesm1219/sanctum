<?php

namespace App\Http\Controllers;

use App\Http\Requests\Agent\CreateAgentRequest;
use App\Http\Requests\Agent\UpdateAgentRequest;
use App\Http\Requests\Auth\AgentLoginRequest;
use App\Models\Address;
use App\Models\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use phpDocumentor\Reflection\Types\Null_;
use Illuminate\Support\Str;

class AgentController extends Controller
{
    public function index()
    {
        try {
            return Agent::all();
        } catch (\Exception  $e) {
            return "Unable to fetch agents.";
        }
    }

    public function login(AgentLoginRequest $request)
    {
        $user = (new AuthController)->agentLogin($request);

        if($user != Null){
            $token = $user->createToken($user)->plainTextToken;
            return response([
                'agent'=>$user,
                'token'=>$token,
                'message' => 'Signed successfully.'
            ], 201);
        }else{
            return response([
                'message' => "Credentials are Incorrect"
            ], 401);
        }
    }

    public function store(CreateAgentRequest $request)
    {
        try {
            $address = (new AddressController)->store($request);
            if($address !== Null){
                // $password = Str::random(12);;
                $password = 'password';
                $request['password'] = $password;
                $agent = $this->storeAgent($request, $address->id);
            }
            // $token = $agent->createToken($agent->id)->plainTextToken;
            return response([
                'agent' => $agent,
                'address' => $address,
                'password' => $password,
                // 'token' => $token,
                'message' => 'Agent Registered Successfully'
            ], 201);
        } catch (\Exception $e) {
            $agent->delete();
            $address->delete();
            return $e;
            return "Sorry, Unable to register your information";
        }
    }

    public function show(Agent $agent)
    {
        try {
            $address = Agent::find($agent->id)->address;
            return [$agent, $address];
        } catch (\Exception $e) {
            // return $e;
            return "Unable to show the Employee";
        }
    }

    public function update(UpdateAgentRequest $request, Agent $agent)
    {
        try {
            $address = (new AddressController)->update($request, $agent->address_id);  
            $agent = $this->updateAgent($request, $agent); 
        } catch (\Exception $e) {
            return "Sorry, Unable to register your address";
        }
        
        // try {
        //     $agent = $this->updateAgent($request, $agent);   
        // } catch (\Exception $e) {
        //     return "Sorry, unable to register your information";
        // }
        
        // return response([
        //     $agent, $address, 
        //     // $token,
        //     'message' => 'Updated Successfully'
        // ], 201);
    }


    // delete specific agent
    public function destroy(Agent $agent)
    {
        $agent->delete();
        return response([
            'message' => 'Agent Deleted Successfully.'
        ]);
    }

     // validate organization's login request
     public function validateLoginRequest()
    {
        return request()->validate([
            'phone' => 'required|string',
            'password' => 'required|string'
        ]);
    }

     // validate organization registration request
     private function validateUpdateAgentRequest()
     {
        return request() -> validate([
            'name' => 'required|string',
            'email' => 'email|required|string|unique:organizations,email|unique:agent,email',
            'password' => 'required|string',
            'phone' => 'required|string|unique:organizations,phone|unique:agent,phone',
            'city' => 'required|string',
            'subcity' => 'required|string',
            'woreda' => 'required|string',
            'longitude' => 'required|string',
            'latitude' => 'required|string',
            'specific_location' => 'string'
        ]);
     }

    private function updateAddress($values, $address)
    {
        if($values['longitude'] == ''){
            $values['longitude'] = Null;
        }
        if($values['latitude'] == ''){
            $values['latitude'] = Null;
        }
        return $address->update([
            'city' => $values['city'],
            'subcity' => $values['subcity'],
            'woreda' => $values['woreda'],
            'longitude' => $values['longitude'],
            'latitude' => $values['latitude'],
            'specific_location' => $values['specific_location'],
        ]); 
    }

    private function storeAgent($values, $address_id)
    {
        return Agent::create([
            'level_id' => $values['level_id'],
            'address_id' => $address_id,
            'first_name' => $values['first_name'],
            'last_name' => $values['last_name'],
            'email' => $values['email'],
            'password' => Hash::make($values['password']),
            'phone' => $values['phone'],
            'vehicle_description' => $values['vehicle_description'],
            'agent_type' => $values['agent_type'],
            'kebele_id' => request()->kebele_id->store('uploads/agents/kebele_id', 'public'),
            'libre' => request()->libre->store('uploads/agents/libre', 'public'),
        ]);
    }
    
    private function updateAgent($values, $agent)
    {
        return $agent->update([
            'level_id' => $values['level_id'],
            'first_name' => $values['first_name'],
            'last_name' => $values['last_name'],
            'email' => $values['email'],
            'password' => Hash::make($values['password']),
            'phone' => $values['phone'],
            'vehicle_description' => $values['vehicle_description'],
            'agent_type' => $values['agent_type'],
            'kebele_id' => request()->kebele_id->store('uploads/agents/kebeleId', 'public'),
            'libre' => request()->libre->store('uploads/agents/libre', 'public'),
        ]);
    }
}
