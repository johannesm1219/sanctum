<?php

namespace App\Http\Controllers;

use App\Models\Awareness;
use Illuminate\Http\Request;

class AwarenessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Awareness  $awareness
     * @return \Illuminate\Http\Response
     */
    public function show(Awareness $awareness)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Awareness  $awareness
     * @return \Illuminate\Http\Response
     */
    public function edit(Awareness $awareness)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Awareness  $awareness
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Awareness $awareness)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Awareness  $awareness
     * @return \Illuminate\Http\Response
     */
    public function destroy(Awareness $awareness)
    {
        //
    }
}
