<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\AdminLoginRequest;
use App\Http\Requests\Employee\CreateEmployeeRequest;
use App\Http\Requests\Employee\UpdateEmployeeRequest;
use App\Models\Address;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class EmployeeController extends Controller
{
    public function index()
    {
        return Employee::all();
    }

    public function store(createEmployeeRequest $request)
    {   
        try {
            $address = (new AddressController)->store($request);
            if($address !== Null){
                // $password = Str::random(12);;
                $password = 'password';
                $request['password'] = $password;
                $role_id = '0d1965f5-761d-49de-bb75-750072f4da5b';
                $employee = $this->createEmployee($request, $address->id, $role_id);
            }
            // $token = $employee->createToken($employee->id)->plainTextToken;
            return response([
                'admin' => $employee,
                'address' => $address,
                'password' => $password,
                // 'token' => $token,
                'message' => 'Employee Registered successfully'
            ], 201);
        } catch (\Exception $e) {
            return $e;
            return "Sorry, unable to create an employee.";
        }
    }

    public function login(AdminLoginRequest $request)
    {
        $user = (new AuthController)->adminLogin($request);

        if($user != Null){
            // $token = $user->createToken($user)->plainTextToken;

            return response([
                'admin'=>$user,
                // 'token'=>$token,
                'message' => 'Signed successfully.'
            ], 201);
        }else{
            return response([
                'message' => "Credentials are Incorrect"
            ], 401);
        }
    }

    public function show(Employee $employee)
    {
        $address = Employee::find($employee->id)->address;
        return [$employee, $address];
    }


    public function update(UpdateEmployeeRequest $request, Employee $employee)
    {
        try {
            $employee->update($request->only([
                'first_name', 'last_name', 'email', 'phone'
            ]));
            return response([
                'employee' => Employee::find($employee->id),
                'message' => 'Updated Successfully'
            ], 201); 
        } catch (\Exception $e) {
            return $e;
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
    }

    private function createEmployee($request, $address_id, $role_id)
    {
        return Employee::create([
            'role_id' => $role_id,
            'address_id' => $address_id,
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'password' => Hash::make($request['password'])
        ]);
    }

    private function storeAvatar($avatar)
    {
        if(request()->has('avatar')){
            return $avatar->update([
                    'avatar' => request()->image->store('uploads/admins/avatar', 'public'),
                ]);
        }
    }
}
