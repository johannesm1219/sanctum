<?php

namespace App\Http\Controllers;

use App\Models\WaterDelivery;
use Illuminate\Http\Request;

class WaterDeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WaterDelivery  $waterDelivery
     * @return \Illuminate\Http\Response
     */
    public function show(WaterDelivery $waterDelivery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WaterDelivery  $waterDelivery
     * @return \Illuminate\Http\Response
     */
    public function edit(WaterDelivery $waterDelivery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WaterDelivery  $waterDelivery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WaterDelivery $waterDelivery)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WaterDelivery  $waterDelivery
     * @return \Illuminate\Http\Response
     */
    public function destroy(WaterDelivery $waterDelivery)
    {
        //
    }
}
