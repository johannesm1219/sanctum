<?php

namespace App\Http\Requests\Organization;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrganizationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            tap(request() -> validate([
                'role_id' => 'required|string',
                'name' => 'required|string',
                'email' => 'email|required|string|unique:organizations,email|unique:agents,email|unique:employees,email',
                'phone' => 'required|string|unique:organizations,phone|unique:agents,phone|unique:employees,phone',
                'city' => 'required|string',
                'subcity' => 'required|string',
                'woreda' => 'required|string',
                'longitude' => 'required|string',
                'latitude' => 'required|string',
                'specific_location' => 'string'
            ]), function(){
                if(request()->hasFile('avatar')){
                    request()->validate([
                        'avatar' => 'file|image|max:3000',
                    ]);
                }
            })
        ];
    }
}
