<?php

namespace App\Http\Requests\RechargePoint;

use Illuminate\Foundation\Http\FormRequest;

class CreateRechargePointRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "agent_id" => 'required|string',
            "recharged_by" => 'required|string',
            "point_recharged" => 'required|string',
        ];
    }
}
