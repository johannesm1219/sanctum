<?php

namespace App\Http\Requests\PickupRequest;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class UpdatePickupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "plastic_id.*" => 'required|string',
            "quantity.*" => 'required|string',
            "unit.*" => 'required|string',
        ];
    }
}
