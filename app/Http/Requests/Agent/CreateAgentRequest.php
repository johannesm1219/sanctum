<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class CreateAgentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            tap(request() -> validate([
                'level_id' => 'required|string',
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                'email' => 'email|required|string|unique:agents,email|unique:organizations,email|unique:employees,email',
                'phone' => 'required|string|unique:agents,phone|unique:organizations,phone|unique:employees,phone',
                'city' => 'required|string',
                'subcity' => 'required|string',
                'woreda' => 'required|string',
                'vehicle_description' => 'required|string',
                'kebele_id' => 'required|file|image|max:3000',
                'libre' => 'required|file|image|max:3000',
                'agent_type' => 'required|string',
                'longitude' => 'string',
                'latitude' => 'string',
                'specific_location' => 'string'
            ]), function(){
                if(request()->hasFile('avatar')){
                    request()->validate([
                        'avatar' => 'file|image|max:3000',
                    ]);
                }
            })
        ];
    }
}
