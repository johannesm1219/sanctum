<?php

namespace App\Http\Requests\Agent;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAgentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'email|required|string|unique:organizations,email|unique:agent,email',
            'password' => 'required|string',
            'phone' => 'required|string|unique:organizations,phone|unique:agent,phone',
            'city' => 'required|string',
            'subcity' => 'required|string',
            'woreda' => 'required|string',
            'longitude' => 'required|string',
            'latitude' => 'required|string',
            'specific_location' => 'string'
        ];
    }
}
