<?php

namespace App\Http\Requests\Notification;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class CreateNotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => 'string|required',
            'body' => 'string|required',
            'role_id' => 'string|required',
            'start_date' => 'string|required|date:',
            'end_date' => 'string|date|after:'. Carbon::today(),
            'status' => 'integer:',
        ];
    }
}
