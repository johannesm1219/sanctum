<?php

namespace App\Http\Requests\Enduser;

use Illuminate\Foundation\Http\FormRequest;

class storeEnduserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "*.enduser_id" => 'required|string',
            "*.plastic_id" => 'required|string',
            "*.quantity" => 'required|float',
            "*.unit" => 'required|string',
        ];
    }
}
