<?php

namespace App\Http\Requests\DeliveryRequest;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class CreateDeliveryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'recycler_id.*' => 'required|string',
            'plastic_id.*' => 'required|string',
            'quantity.*' => 'required|string',
            'price.*' => 'required|string',
            'unit.*' => 'required|string'
        ];
    }
}
