<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    use Uuids;
    use HasFactory;

    protected $guarded = [];

    public function agent()
    {
        return $this->hasMany(Agent::class);
    }
}
