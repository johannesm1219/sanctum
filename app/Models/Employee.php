<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

class Employee extends Model
{
    use Uuids;
    use HasFactory;
    use HasApiTokens;

    protected $guarded = [];

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function notification()
    {
        return $this->hasMany(Notification::class);
    }

    public function awareness()
    {
        return $this->hasMany(Awareness::class);
    }

    public function rechargePoint()
    {
        return $this->hasMany(RechargePoint::class);
    }

    public function stockMarket()
    {
        return $this->hasMany(StockMarket::class);
    }
}
