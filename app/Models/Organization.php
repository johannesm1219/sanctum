<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

class Organization extends Model
{
    use Uuids;
    use HasFactory;
    use HasApiTokens;

    protected $guarded = [];

    protected $hidden = [
        'password', 
        'remember_token',
        'created_at',
        'updated_at',
        'address_id'
    ];

    public function enduserRequest()
    {
        return $this->hasMany(EnduserRequest::class, 'enduser_id', 'id');
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }
    
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function recyclerRequest()
    {
        return $this->hasMany(RecyclerRequest::class, 'id', 'recycler_id');
    }

    public function rating()
    {
        return $this->hasMany(Rating::class);
    }
}
