<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EnduserRequest extends Model
{
    use HasFactory;
    use Uuids;

    protected $guarded = [];

    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'id', 'enduser_id');
    }

    public function plastic()
    {
        return $this->belongsTo(Plastic::class);
    }

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = [
    //     'updated_at',
    //     'created_at',
    // ];
}
