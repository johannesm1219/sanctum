<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

class Agent extends Model
{
    use Uuids;
    use HasFactory;
    use HasApiTokens;

    protected $guarded = [];

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function enduserRequest()
    {
        return $this->hasMany(EnduserRequest::class);
    }

    public function recyclerRequest()
    {
        return $this->hasMany(RecyclerRequest::class);
    }

    public function level()
    {
        return $this->belongsTo(Level::class);
    }
    
    public function notification()
    {
        return $this->hasMany(Notification::class);
    }

    public function rechargingPoint()
    {
        return $this->hasMany(RechargePoint::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function rating()
    {
        return $this->hasMany(Rating::class);
    }
}
