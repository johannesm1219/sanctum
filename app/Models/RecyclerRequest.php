<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecyclerRequest extends Model
{
    use Uuids;
    use HasFactory;

    protected $guarded = [];

    public function agent()
    {
        return $this->belongsTo(Agent::class);
    }

    public function RecyclerAddress()
    {
        return $this->hasOneThrough(Address::class, Organization::class, 'address');
    }

    public function plastic()
    {
        return $this->belongsTo(Plastic::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'recycler_id', 'id' );
    }
}
