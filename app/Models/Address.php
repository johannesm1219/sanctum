<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use Uuids;
    use HasFactory;

    protected $guarded = [];

    public function agent()
    {
        return $this->hasOne(Agent::class);
    }

    public function recyclerRequestAddress()
    {
        return $this->hasOneThrough(RecyclerRequest::class, Organization::class, 'address_id', 'recycler_id', 'id', 'id');
    }

    public function organization()
    {
        return $this->hasOne(Organization::class)
        ->withPivot('name', 'email');
    }

    public function employee()
    {
        return $this->hasOne(Employee::class);
    }
}
