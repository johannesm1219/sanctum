<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StockMarket extends Model
{
    use Uuids;
    use HasFactory;

    protected $guarded = [];

    public function plastic()
    {
        return $this->belongsTo(Plastic::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
