<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use Uuids;
    use HasFactory;

    protected $guarded = [];

    public function agent()
    {
        return $this->hasMany(Agent::class);
    }

    public function employee()
    {
        return $this->hasMany(Employee::class);
    }

    public function organization()
    {
        return $this->hasMany(Organization::class);
    }
}
