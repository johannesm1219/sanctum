<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plastic extends Model
{
    use Uuids;
    use HasFactory;

    protected $guarded = [];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function recyclerRequest()
    {
        return $this->hasMany(RecyclerRequest::class);
    }

    public function enduserRequest()
    {
        return $this->hasMany(EnduserRequest::class);
    }

    public function stockMarket()
    {
        return $this->hasMany(StockMarket::class);
    }
}
